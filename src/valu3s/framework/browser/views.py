from Products.Five.browser import BrowserView
from plone.dexterity.browser.view import DefaultView
from operator import itemgetter
from plone import api
from Products.statusmessages.interfaces import IStatusMessage
import logging

logger = logging.getLogger("Joseba11")

class DemoView(BrowserView):


    def the_title(self):

        return u'A list of great trainings:'
    def talks(self):

        results = []

        data = [

            {'title': 'Dexterity is the new default!',

             'subjects': ('content-types', 'dexterity')},

            {'title': 'Mosaic will be the next big thing.',

             'subjects': ('layout', 'deco', 'views'),

             'url': 'https://www.youtube.com/watch?v=QSNufxaYb1M'},

            {'title': 'The State of Plone',

             'subjects': ('keynote',)},

            {'title': 'Diazo is a powerful tool for theming!',

             'subjects': ('design', 'diazo', 'xslt')},

            {'title': 'Magic templates in Plone 5',

             'subjects': ('templates', 'TAL'),

             'url': 'http://www.starzel.de/blog/magic-templates-in-plone-5'},

        ]

        for item in data:

            url = item.get('url', 'https://www.google.com/search?q={}'.format(item['title']))

            talk = {

                'title': item['title'],

                'subjects': ', '.join(item['subjects']),

                'url': url

                }

            results.append(talk)

        return sorted(results, key=itemgetter('title'))

    def context_info(self):

        context = self.context
        title = context.title
        portal_type = context.portal_type
        url = context.absolute_url()
        return u"This is the {0} '{1}' at {2}".format(portal_type, title, url)


class SomeOtherView(BrowserView):


    def __call__(self):

        portal = api.portal.get()

        some_talk = portal['dexterity-for-the-win']

        training_view = api.content.get_view('training', some_talk, self.request)
        return training_view.context_info()


class TalkView(DefaultView):

    """ The default view for talks"""


class TalkListView(BrowserView):

    """ A list of talks

    """
    criteria='*'

    def update(self):

        if "button-name" in self.request.form:

            messages = IStatusMessage(self.request)
            try:
                # do something
                messages.add("Button pressed")
                filename = self.request.form['filename']
                self.criteria = filename
                logger.info('Call froga:::::::' + self.criteria)
                logger.info('froga:::::::' + self.criteria)
                

            except Exception as e:
                logger.exception(e)
                messages.add(u"It did not work out. This exception came when processing the form:" + unicode(e))


    def talks(self):

        results = []
        brains = api.content.find(context=self.context, portal_type='talk', type_of_talk='Talk')
        for brain in brains:
            talk = brain.getObject()
            results.append({
                'title': brain.Title,
                'description': brain.Description,
                'url': brain.getURL(),
                'audience': ', '.join(talk.audience),
                'type_of_talk': talk.type_of_talk,
                'speaker': talk.speaker,
                'room': talk.room,
                'uuid': brain.UID,
                })
        return results
