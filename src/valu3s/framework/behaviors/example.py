# -*- coding: utf-8 -*-

from plone.autoform.interfaces import IFormFieldProvider

from plone.supermodel import directives

from plone.supermodel import model
from z3c.relationfield.schema import RelationChoice
from z3c.relationfield.schema import RelationList
from zope.schema.vocabulary import SimpleVocabulary

from plone.autoform import directives as form
from z3c.form.browser.checkbox import CheckBoxFieldWidget
from z3c.form.browser.radio import RadioFieldWidget
from z3c.form.browser.orderedselect import OrderedSelectWidget
#from plone.formwidget.autocomplete import AutocompleteMultiFieldWidget
from plone.z3cform.textlines import TextLinesFieldWidget	
from z3c.form.browser import select
	


from zope import schema

from zope.interface import provider


@provider(IFormFieldProvider)
class IFramework(model.Schema):


    directives.fieldset(

        'framework',

        label=u'Framework',

        fields=['framework','firstname', 'country', 'yourField', 'zerrendaBox', 'zerrendaRadio','zerrendaSelect'],# 'zerrendaMultiSelect'],

    )

   
   
    framework = schema.Bool(

        title=u'Framework SXXXX how this item on the frontpage',

        required=False,

    )

    firstname = schema.TextLine(title=(u"First name"), default=u"")

    #vocabulary=SimpleVocabulary.fromValues([u'foo',u'bar']))
    country = schema.Choice( title = (u"Country"),
        vocabulary = SimpleVocabulary.fromValues([u'foo',u'bar']) , required=False, default=None)

    #form.widget(yourField=CheckBoxFieldWidget)
    yourField = schema.List(title=u"Available headers and animations",
                               description=u"Headers and animations uploaded here",
                               required=False,
                               value_type= schema.Choice( title = (u"Country"),
                                    vocabulary = SimpleVocabulary.fromValues([u'foo',u'bar']) , required=False, default=None),
                               )
    
    form.widget(zerrendaBox=CheckBoxFieldWidget)
    zerrendaBox = schema.List(title=u"Available headers and animations",
                               description=u"Headers and animations uploaded here",
                               required=False,
                               value_type= schema.Choice( title = (u"Country"),
                                    vocabulary = SimpleVocabulary.fromValues([u'foo',u'bar']) , required=False, default=None),
                               )
    
    form.widget(zerrendaRadio=RadioFieldWidget)
    zerrendaRadio = schema.List(title=u"Available headers and animations",
                               description=u"Headers and animations uploaded here",
                               required=False,
                               value_type= schema.Choice( title = (u"Country"),
                                    vocabulary = SimpleVocabulary.fromValues([u'foo',u'bar']) , required=False, default=None),
                               )
    
  
    zerrendaSelect = schema.Choice( title = (u"Country"),
                                    vocabulary = SimpleVocabulary.fromValues([u'foo',u'bar']) , required=False, default=None)
                               
    '''
    form.widget(zerrendaMultiSelect=select.SelectWidget)
    zerrendaMultiSelect =  schema.List(title=u"Available headers and animations",
                               description=u"Headers and animations uploaded here",
                               required=False,
                               value_type= schema.Choice( title = (u"Country"),
                                    vocabulary = SimpleVocabulary.fromValues([u'foo',u'bar']) , required=False, default=None),
                               )
    
    form.widget(zerrendaMultiSelect=select.SelectWidget)
    zerrendaMultiSelect = schema.Choice( title = (u"Country"),
                                    vocabulary = SimpleVocabulary.fromValues([u'foo',u'bar']) , required=False, default=None)
                        
    '''