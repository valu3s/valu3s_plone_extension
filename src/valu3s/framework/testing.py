# -*- coding: utf-8 -*-
from plone.app.contenttypes.testing import PLONE_APP_CONTENTTYPES_FIXTURE
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import (
    applyProfile,
    FunctionalTesting,
    IntegrationTesting,
    PloneSandboxLayer,
)
from plone.testing import z2

import valu3s.framework


class Valu3SFrameworkLayer(PloneSandboxLayer):

    defaultBases = (PLONE_APP_CONTENTTYPES_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        import plone.restapi
        self.loadZCML(package=plone.restapi)
        self.loadZCML(package=valu3s.framework)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'valu3s.framework:default')


VALU3S_FRAMEWORK_FIXTURE = Valu3SFrameworkLayer()


VALU3S_FRAMEWORK_INTEGRATION_TESTING = IntegrationTesting(
    bases=(VALU3S_FRAMEWORK_FIXTURE,),
    name='Valu3SFrameworkLayer:IntegrationTesting',
)


VALU3S_FRAMEWORK_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(VALU3S_FRAMEWORK_FIXTURE,),
    name='Valu3SFrameworkLayer:FunctionalTesting',
)


VALU3S_FRAMEWORK_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        VALU3S_FRAMEWORK_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE,
    ),
    name='Valu3SFrameworkLayer:AcceptanceTesting',
)
