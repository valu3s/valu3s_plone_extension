# valu3s_framework

This project contains the VALU3S framework implementation as a Plone Extension.

This extension contains the following functions/features:

1- It contains the definition of the different elements of the VALU3S Framework as Dexterity types. In this way Plone allows to create the different elements of the framework and their visualisation. 

2- It offers an **advanced search view for use cases**. This view can be used as a tutorial to develop new views.

3- The extension also offers the **characterisation of any element based on the dimensions and layers** of the VALU3S Framework, for which a **Plone Behavior** has been created that can be applied to any Dexterity type. 

This repository has two objectives.  On the one hand, it allows to automatically update the VALU3S WEB REPO production server with the new functionalities developed. On the other hand, it explains and offers a local Dockerised development environment for developers.


# Pre-requisites

[Docker](https://docs.docker.com/engine/install/)

[Docker-compose](https://docs.docker.com/compose/install/)




# Development environment

This section  shows how to install the Dockerized developmentent environemnet.

First download the Docker-compose file  with the volumes that are a snapshot (2021-11-10) of the VALU3S Web Repo server filesystem with its Zope database.

[Link to VALU3S Web Repo Docker environment 2021-11-10](https://eden.dei.uc.pt/~fmduarte/plone-compose-10Nov.zip) 

https://eden.dei.uc.pt/~fmduarte/plone-compose-10Nov.zip

Unzip the file, and you will find the Plone server configuration file (buildout.cfg), the docker compose file,  the VALU3S plone server data folder with the database data and the VALU3S  Plone Extension code. 

```bash
~/src/valu3s/development_environment$ ls 
buildout.cfg  data  docker-compose.yml  valu3s_framework
```

[The VALU3S  Plone server](http://valu3s.dei.uc.pt/)   configuration is the next one:

- Plone 5.2.2 (5209)
- CMF 2.4.8
- Zope 4.5.1
- Python 2.7.18 (default, Apr 20 2020, 19:34:11) [GCC 8.3.0]

To run the server execute one of the above commands.

```bash
~/src/valu3s/development_environment$ docker-compose up -d
Starting valu3s_plone_1 ... done
```

```bash
~/src/valu3s/development_environment$ docker-compose up 
Creating valu3s_plone_1 ... done
Attaching to valu3s_plone_1
plone_1  | Getting distribution for 'mr.developer==2.0.0'.
plone_1  | Got mr.developer 2.0.0.
plone_1  | Develop: '/plone/instance/src'
plone_1  | Getting distribution for 'col
.....
plone_1  | 2022-02-21 09:47:39,322 INFO    [Zope:45][MainThread] Ready to handle requests
```

To stop the server run 
```bash
~/src/valu3s/development_environment$ docker-compose stop 
Stopping valu3s_plone_1 ... done
```


Now browse to [localhost:8080/repo](localhost:8080/repo) and check that the server is working. This server has the same users as the VALU3S  web repo, so VALU3S parteners can use their credentials. But there is also an admin fake  user with credentials, just for development purposes.

- user=admin
- password=VALU3Sadmin


Now that the server is running  let's do a short validation to check that the server replicates the VALU3S Web repo. First check the view of methods and use cases. Then check the correct installation of **VALU3S EXTENSION**, for that follow the process that shows the next figures:


Browse to [localhost:8080/repo](localhost:8080/repo) and login. Then click in the admin user and select site setup.

<img src="./docs/images/admin_menu.png" alt="Admin menu" width="50%" height="50%"/>


No click in the **Add-ons** option and check that the **VALU3S EXTENSION** is installed.

<img src="./docs/images/admin_menu1.png" alt="Admin menu1" width="50%" height="50%"/>
<img src="./docs/images/admin_menu2.png" alt="Admin menu 2" width="50%" height="50%"/>

Once you have checked the installation of the extension, navigate to the dexterIty types menu and check the properties of  **Use cases** types. Check that the **uses cases** have the VALU3S behaviour enabled. 

<img src="./docs/images/dexterity_menu.png" alt="Dexterity menu" width="50%" height="50%"/>
<img src="./docs/images/dexterity_menu1.png" alt="Dexterity menu1" width="50%" height="50%"/>
<img src="./docs/images/dexterity_menu2.png" alt="Dexterity menu2" width="50%" height="50%"/>
<img src="./docs/images/dexterity_menu3.png" alt="Dexterity menu3" width="50%" height="50%"/>


# Plone Extension Development 

Once the development environment is installed, this section explains how to add and develop new functionalities, mainly **searching views and behaviours**, to the **VALU3S Plone extension**.

As IDE you can use anyone that works well for python. For example Visual Code, PyCharm, sublime...

To allow modifying the extension code while the plone server container is running you should modify **valu3s_framework/** folder permissions. First create a Plone group (gorup Id 500) and add your user to that griup. Finally modify the permissions of the folder with write permissions for the Plone user group.


```bash
$ sudo addgroup --gid 500 plone
$ sudo usermod -a -G 500 your_user_name
$ chmod 775 -R valu3s_framework/
```

## Updating Zope portal catalog Indexes

In plone, database index and search facilities are provided by [portal_catalog](https://docs.plone.org/external/plone.app.dexterity/docs/advanced/catalog-indexing-strategies.html) tool. [Indexing](https://docs.plone.org/develop/plone/searching_and_indexing/indexing.html) is the action to make object data search-able. Plone stores available indexes in the database. There are two distinct functions. You may have two different interests in regard to indexing your custom content type objects:

- Making particular fields searchable via Plone’s main search facility;

- Indexing particular fields for custom lookup.

You can create them through-the-web and inspect existing indexes in portal_catalog on Index tab.The Catalog Tool can be configured through the Management Interface or programatically in Python but current best practice in the CMF world is to use GenericSetup to configure it using the declarative catalog.xml file. In this how to we will create the indexes through the web and also will show ho to import a complete portal catalog form a existing server with all its indexes.

In the next two subsection we will show how to create indexes that then we will use to create Plone's search functionalities and views. 
In the next examples  an index of  a field of the VALU3S Plone Bhevaiour. Will be used. The idea is once the Index is created use is it in th efacteed navigation of the use case.  To achieve this the process is the next one:

1- Apply the Behaviur to a content type
2- Create an index based on a field of the behaviour 
3- Use the index to perform a Faceted navigation of the type based on the index/field

The next figure shows the VALU3S Behaviour Fields. To browse to this page first click on a use case , then select 'Edit' and the 'VALU3S Framework' tab will be showed as in the figure. Using the 'VALU3S Framework' Behaviour tab you can characterize the element with layers and dimension of the framework. For exmaple in the figure UC2 has been characterize as 'In the Lab' Evaluation Environment type. 

<img src="./docs/images/valu3s_framework_behaviour_edit.png" alt="Valu3s Framework Behaviour" width="50%" height="50%"/>


---
### Creating an Index in Plone's Portal Catalog

In this section we want to create a Plone index of the Evalaution Enviroment Type field of the VALU3S Framework Behaviour. 

1- Go to 'Site Setup'
2- Select 'Management Interface' option
3- Click on 'Portal Catalog'
4- Select 'Indexes' tab
5- Click 'Add' Index  selecting KeywordIndex 
6- Create the index based on the 'valu3s_evaluation_environment' field of the Behaviour a named it with the next Id 'field_valu3s_evaluation_environment'
7-Once the index is created select it
8- And now click 'Reindex'

Now we are ready to  create a Faceted based query using of this field. For example we can go to use case select the enviroment type and the do the searching.




1- Go to 'Site Setup'

<img src="./docs/images/menu_site_setup.png" alt="Menu Site Setup" width="50%" height="50%"/>

2- Select 'Management Interface' option

<img src="./docs/images/zope_management.png" alt="Menu Managment interface" width="50%" height="50%"/>


3- Click on 'Portal Catalog'

<img src="./docs/images/portal_catalog.png" alt="Portal catalog" width="50%" height="50%"/>


4- Select 'Indexes' tab

<img src="./docs/images/indexes.png" alt="Portal catalog Indexes" width="50%" height="50%"/>



5- Click 'Add' Index  selecting KeywordIndex 

<img src="./docs/images/add_new_index.png" alt="Add Index" width="50%" height="50%"/>

6- Create the index based on the 'valu3s_evaluation_environment' field of the Behaviour a named it with the next Id 'field_valu3s_evaluation_environment'

<img src="./docs/images/creating_keyword_inex.png" alt="Creating keyword index" width="50%" height="50%"/>


7-Once the index is created select it

<img src="./docs/images/index_created.png" alt="Created keyword index" width="50%" height="50%"/>

8- And now click 'Reindex'

<img src="./docs/images/index_reindex.png" alt="Created keyword index" width="50%" height="50%"/>

### Create faceted navigation for use cases using the VALU3S behaviour based on the multidimensional V&V Framework


Now that we have verified that the extension is installed correctly and we have created an Index of a field of the VALU3S Framework Behaviour  we can create a Fectede navigation for that index.

To do this:


 1- first we will characterize a Use Case with that field.For exmaple in the figure UC2 has been characterize as 'In the Lab' Evaluation Environment type. 
 2- Then Click on the Uses Cases Folder
 3- And now select the 'fFactede navigation' menu's Confirutaion option. We will create here the Facated navigation
 4- Now click 'Add a widget', for example in the Right Pane
 5- And we will configure the widget for a seraching view based on the index that we have created.
     - Select as the widget type 'CheckBoxes'
     - Cretae a name for the widget 'Valu3s Evaluation Environment'
     - The select the Catalog Index to use in the search, in our case the one that we have cerated called 'field_valu3s_evaluation_environment'
     - Default operator 'Or'
     - Catalog property must be set to True
6-we now have the search widget created. It is important to note that if no use case has been characterised with the field, no search options will appear, and only the values that have been used will appear. In our case , for the moment, we have only used 'in the lab' option , but if more values are used those values will appear ;-)
 
7- Finally we can filter Use Cases using this field value. Click in the 'Use cases' folder and use the widget. Don't forget to reindex the protal catalog once you have chacrerized th euse cases with the field;-)

 

1- First we will characterize a Use Case with that field.For exmaple in the figure UC2 has been characterize as 'In the Lab' Evaluation Environment type. 

<img src="./docs/images/valu3s_framework_behaviour_edit.png" alt="Valu3s Framework Behaviour" width="50%" height="50%"/>


3- And now select the 'fFactede navigation' menu's Confirutaion option. We will create here the Facated navigation

<img src="./docs/images/faceted_config.png" alt="Facated config" width="50%" height="50%"/>

4- Now click 'Add a widget', for example in the Right Pane

<img src="./docs/images/facated_add_widget.png" alt="Faceted add widget" width="50%" height="50%"/>

5- And we will configure the widget for a seraching view based on the index that we have created 

<img src="./docs/images/widget_config.png" alt="Faceted widget config" width="50%" height="50%"/>

6-we now have the search widget created

<img src="./docs/images/widget_valu3s.png" alt="Faceted widget  valu3s" width="50%" height="50%"/>

<img src="./docs/images/widget_empty.png" alt="Faceted widget  valu3s" width="50%" height="50%"/>

<img src="./docs/images/widget_query.png" alt="Faceted widget  valu3s" width="50%" height="50%"/>

### Importing a Portal catalog

In the previous section how to create an index and how to use it to create a fectede navigation has been showed.Portal catalog  Index creation can be done either manually or programmatically. Manual creation can be very cumbersome. It is  possible to  import the entire portal catalogue directly.

To import an entire portal catalogue index you have to do the following:

- Import production server portal catalog. When importing the catalog portal it will be downloaded as a .zexp file.

- Once the .zexp file is in the developer local machine create the folder mkdir  'import' inside 'data/' of the working directory

- Copy the .zexp file to the 'data/import' folder
- Modify the docker-compose.yml to include the 'data/import' folder as a volumen
- Once the container is running connect to its bash  
  - docker exec -it valu3s_plone_1 bash
- Now inside the docker container create the next link to the volumen
  - ln -s /data/instance/import/ /plone/instance/var/import
- Now go to the browser go to the "manage Interface" of Zope ,  delete the portal catalog and import the new one

You have lodaded the catalog.Reindex the catalog and you are ready to cretae the new views. 


## Adding a View

Now that we know what is a Plone Index and how to use it to taylor a Faceted Navigation Widget, let's create a more advanced view.The code of this view is inside this repository. As an example we will create  a custom  view that permits searching Use cases by domain and Methods.  In Plone to create custom views and searching forms we need to use a Plone  Extension. This views not only has the property that allows to do the searching by domain and methods, it also presents all  use case data  in a navegable custom table that shows more fields than the faceted navigation tables and than the default view table. For example in this custom view  the methods,evaluation scenarios and domains of each use case are shown.

The Plone extensions/compoennets has a concrete structure. For example the views must be developed in the next folder:

```bash
valu3s_framework/src/valu3s/framework/browser/
```


To develop a new view the developer must deal at least with 3 files inside the 'Browser'  folder:

- templates/
- view.py python script
- configure.zcml Plone configuration file

In the case of the example view that we are using  the files are the next ones

- **templates/usecases_search_view.pt**
- **usecases_search_view.py**
- **configure.zcml**

To create any new views you can copy the first two files (.pt and .py), rename them and use then as a starting point to create a new view. Once the files are created you should modify also configure.zcml to add the view to plone. Once this is done the view must be related in plone to an element.

To explain all of these  fisrt we will explain a a little bit the main concepts of **usecases_search_view.pt** and **usecases_search_view.py** source files. 

The next step will to activate this views in Plone.

And finally we will show how to modify the code.

Anyway i good startung point to learn how to develop plone's view could be the next link [Mastering Plone](https://training.plone.org/5/mastering-plone-5/) 


### Use cases searching by domain and V&V methdos

The aim  of **usecases_search_view.pt** file is to define the HTML view/page. This template is processed in the server so it can include embedded python script to generat ethe final HTML view. In our case for example we have the next important code snippet where we call a python function that returns all the use cases of Plone and create a list variable called use_cases with all the information. This var could be used through all the page. But as we want to show in a navigable html table we will cpoy to  the batch variable teh element seven by seven. That way we will show a table of 7 rows:

```html
 <div metal:fill-slot="main"
        tal:define="use_cases python:view.use_cases();
                    Batch python:modules['Products.CMFPlone'].Batch;
                    b_size python:int(request.get('b_size', 7));
                    b_start python:int(request.get('b_start', 0));
                    batch python:Batch(use_cases,b_size,b_start);">
```

Then we can use the **batch** variable to create and show a table with all the use cases. for that a loop is created and **element** is the iterate element with the info of a use case.

```html
    <tr tal:repeat="element batch">
```

And now we can use in the HTML colummns the element properties:

```html
        <td tal:content="python:element['domain']">
            No domain defined
        </td>

        <td tal:content="python:element['evaluation_scenarios']">
            No scenarios
        </td>
        <td tal:content="python:element['use_case_methods']">
           No description
        </td>
        <td tal:content="python:element['provider_ref']">
```

Now that we have the code that shows the use cases data in the HTML page , let's see the python code that obtains the use cases **usecases_search_view.py**. In our case the python function that obtains/query all the use cases is the function **use_cases()** of the **class UseCasesView(BrowserView):** class located in **usecases_search_view.py**. This function find all the use cases element of the Plone CMS with next sentences **api.content.find(portal_type='use_case')**. But it can also use filtering params using the next function call **brains = api.content.find( portal_type='use_case', \*\*params)**  where params is  a index of the portal catalog that is related with a field of the use cases elements: 

- valu3s_use_case_domain (indexed attributes: use_case_domain)  being **use_case_domain** a property of the Use Case Dexterity Type

Let's remark also that to obtain the methods related to a use case the **self.use_case_methods(use_case)** method is used. This method iterate directly the field of the obtained use case content elements.


```python
class UseCasesView(BrowserView):
    ...
      def use_cases(self):
        results = []
        params={}
        if not self.criteria_domain is None and  not self.criteria_domain == 'any':
            params['valu3s_use_case_domain']= self.criteria_domain
        
        if len(params) == 0:
            brains = api.content.find(portal_type='use_case')      
        else:
            brains = api.content.find( portal_type='use_case', **params)          
        for brain in brains:
            use_case = brain.getObject()
            logger.info('USE CASE::::::'+str(use_case.use_case_number))            
            if self.hasQueryMethod(use_case):
                results.append({
                    'title': brain.Title,
                    'description': brain.Description,
                    'url': brain.getURL(),
                    'use_case_number': use_case.use_case_number,
                    #'domain': ', '.join(use_case.domain or []),
                    'evaluation_scenarios': ', '.join(self.scenarios(use_case) or []),
                    'use_case_methods': ', '.join(self.use_case_methods(use_case) or []),
                    'domain': use_case.use_case_domain,
                    'provider_ref': self.provider(use_case)
                    })
        return results
```

This function uses also the criteria/filters used to search use case by domain or/and V&V method. To specify the searching criterias this Pyhton BrowserView class has an **update()** function that is called each time the page is loaded. For this the html page **usecases_search_view.pt** has a form to specify the method and domain.

 **usecases_search_view.pt** 
```html

<div class="faceted-form" id="faceted-form" data-mode="view">
<!-- Top -->
  <div id="top-area" class="faceted-area top-area">
    <div id="faceted-top-column" class="faceted-column faceted-top-column visualNoPrint">
      <div id="top---default---widgets" class="faceted-drag-drop faceted-widgets faceted-default-widgets faceted-top-widgets">
      <form action="./search-use-case-by-relationships" method="POST">
       <div class="faceted-widget"  id="c6_widget">
       <fieldset class="widget-fieldset">
            <legend>Use Case Domain</legend>
            <label class="select-field" for="c6">Use Case Domain</label>
                <select id="c6" class="searchField"  title="Select Domain" name="domain" tal:define="domains python:view.domains()">
                <option value="any" selected="selected">Select Domain</option>
                <option tal:repeat="domain domains"
                    tal:content="python:domain"/>
              </select>
       </fieldset>
       </div>

```

And whit the next script in the html each time the page is loaded we call th eupdate function:

```html
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"
      metal:use-macro="context/main_template/macros/master"
      i18n:domain="valu3s.framework"
      tal:define="kaixo view/update">
```

To filter by method also the nex  HTML form select field is used in the .pt field. In this case to obtain the methods list the **methods()** function of the **class UseCasesView(BrowserView):** class is used.

```html
  <div class="faceted-widget"  id="c7_widget">
  <fieldset class="widget-fieldset">
    <legend>VALU3S V&V Methods</legend>
      <label class="select-field" for="c7">Methods</label>
      <select class="faceted_select" name="method" id="c7" tal:define="methods python:view.methods()">
        <option title="All" value="any" selected="selected">All</option>
      <!--TODO: Programatically  This better to be done plone way-->
        <option tal:repeat="method methods"
                    tal:content="python:method['title']"/>
      </select>
  </fieldset>
```

The python **methods()** is the next one. It just retrives from Plone all the methods created in Plone using **api.content.find(portal_type='method')**.

```python
    def methods(self):
        results = []
        brains = api.content.find(portal_type='method')
        for brain in brains:
            method = brain.getObject()
            logger.info('METHOD:'+str(method.title))
            results.append({
                'title': brain.Title,
                'description': brain.Description,
                'url': brain.getURL()
                })
        return results


```

And is the **update()** function  the one that  receives and established the  searching criterias (domains and methods):

```python
    def update(self):

         if "speaker-filter" in self.request.form:
            messages = IStatusMessage(self.request)
            try:
                self.criteria_domain = self.request.form['domain']
                self.criteria_method = self.request.form['method']
                #logger.info('criteria:::::::' + self.criteria)
                messages.add('criteria:::::::' + self.criteria_method + '::::::' + self.criteria_domain)
            except Exception as e:
                logger.exception(e)
                messages.add(u"It did not work out. This exception came when processing the form:" + unicode(e))

```
The rest of the python code in **usecases_search_view.py**  is basic python code that works with list and arrays to obtain the use cases data.

Finally using the **configure.zcml** the view is configured. In the next code snippet which is the view template and which is the viw python class is said. It is declared also which will be the view name **usecases_view**,   this name is used in the url.

```xml
 <browser:page
          for="*"
          name="usecases_view"
          layer="zope.interface.Interface"
          permission="zope2.View"
          class=".usecases_search_view.UseCasesView"
          template="templates/usecases_search_view.pt"
    />
```

The last step to create the view is to define which are the views that we can use iwith a folder. This is speicified in the file **src/valu3s/framework/content/profiles/default/types/folder.xml**


```xml
<?xml version="1.0"?>

<object name="Folder">
 <property name="view_methods" purge="False">
  <element value="yourviewname"/>
  <element value="usecases_view"/>
 </property>
</object>
```


### Activating the view in Plone Web Repo

Before applying the view  that filter use cases by domain and methods the domain index must be created in the portal catalog. To do this repeat the process that we showed previously. You should add the  **valu3s_use_case_domain (indexed attributes: use_case_domain)** Keyword index to the portal_catalog. Use the index is created don't forget to reindex it;-)

<img src="./docs/images/domain_keyword_index.png" alt="Domain index" width="50%" height="50%"/>

Now using the next url we can call the view **http://localhost:8080/repo/use_cases/usecases_view**. Remember that the code of the extension should be teh actual version, so don't forget to update the local repo  **git pull origin master**. 

<img src="./docs/images/usecases_view.png" alt="Domain index" width="50%" height="50%"/>


Now the view is working if we click the button  **filter** there will be an error due to an incorrect URL. To fix this error create a folder  under use cases called , for example **Search use cases by relationship**   and asociated it to our new custom. 

<img src="./docs/images/use_case_view_default.png" alt="Domain index" width="50%" height="50%"/>

Now we will see by default our new view.And now we can see the query/filer  result.

<img src="./docs/images/use_case_view_filtering.png" alt="Domain index" width="50%" height="50%"/>


### Modifying the view code

If we mnodify the code to update the server just stop and run again the server and the extension will be update. You can also do a git pull to obtain the last code or exeute buildout.


## Adding a behavior

The [Plone behaviors](https://docs.plone.org/external/plone.app.dexterity/docs/behaviors/index.html) are re-usable bundles of functionality that can be enabled or disabled on a per-content type basis. Examples might include:

- A set of form fields (on standard add and edit forms),
- Enabling particular event handler,
- Enabling one or more views, viewlets or other UI components,
- Anything else which may be expressed in code via an adapter and/or marker interface.

You would typically not write a behavior as a one-off. Behaviors are normally used when:

- You want to share fields and functionality across multiple types easily. Behaviors allow you to write a schema and associated components (e.g. adapters, event handlers, views, viwelets) once and re-use them easily.

- A more experienced developer is making functionality available for re-use by less experienced integrators. For example, a behavior can be packaged up and release as an add-on product. Integrators can then install that product and use the behavior in their own types, either in code or through-the-web.

 This manual describes a little bit the VALU3S behaviors in case we need to create more behaviors during theproject.

 To create a Plone  behaviour two files must be used inder the 'Behavior' folder. 
 
 ```bash
valu3s_framework/src/valu3s/framework/behaviors/
```

 The firts one is the definition of the behavoiour , in our case  **framework.py**. If we need a new behavior a new .py file should be created. The second file that we need is the behavior Plone/Zope configuration file under this folder **configuration.zcml** 

### framework.py
The idea of a behaviour is define a class with fields and associate teh fields with a view widget.The best way to understand how to implement a behaviour is to analyza **framework.py** code. Here is a snippet of it:

```python
@provider(IFormFieldProvider)
class IFramework(model.Schema):


    directives.fieldset(

        'VALU3S framework',

        label=u'VALU3S Framework',

        fields=['valu3s_evaluation_environment', 'valu3s_evaluation_type', 'valu3s_type_of_component_under_evaluation',
                'valu3s_evaluation_stage', 'valu3s_purpose_of_the_component_under_evaluation', 
                'valu3s_type_of_requirement_under_evaluation','valu3s_evaluation_performance_indicator'],

    )

   
    form.widget(valu3s_evaluation_environment=CheckBoxFieldWidget)
    valu3s_evaluation_environment = schema.List(
        title=u'Evaluation Environment Type',
        description=u"Type of environment where the method is applied. It can be more than one type of environment.(Dimension 1)",
        required=False,
        value_type= schema.Choice( title = (u"Dimnension1"),
                    vocabulary = SimpleVocabulary.fromValues([u'In-the-lab environment',u'Closed evaluation environment',u'Open evaluation environment']) ,
                    required=False, default=None),
        
    )

```

### configuration.zcml
The last step to actic¡vate the behaviour is configure **configure.zcml**.


```xml
  <plone:behavior
      title="VALU3S Framework"
      name="valu3s.framework"
      description="VALU3S project Multidimensional framework labels"
      provides=".framework.IFramework"
      />
```






