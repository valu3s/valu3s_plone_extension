.. This README is meant for consumption by humans and pypi. Pypi can render rst files so please do not use Sphinx features.
   If you want to learn more about writing documentation, please check out: http://docs.plone.org/about/documentation_styleguide.html
   This text does not appear on pypi or github. It is a comment.

.. image:: https://travis-ci.org/collective/valu3s.framework.svg?branch=master
    :target: https://travis-ci.org/collective/valu3s.framework

.. image:: https://coveralls.io/repos/github/collective/valu3s.framework/badge.svg?branch=master
    :target: https://coveralls.io/github/collective/valu3s.framework?branch=master
    :alt: Coveralls

.. image:: https://img.shields.io/pypi/v/valu3s.framework.svg
    :target: https://pypi.python.org/pypi/valu3s.framework/
    :alt: Latest Version

.. image:: https://img.shields.io/pypi/status/valu3s.framework.svg
    :target: https://pypi.python.org/pypi/valu3s.framework
    :alt: Egg Status

.. image:: https://img.shields.io/pypi/pyversions/valu3s.framework.svg?style=plastic   :alt: Supported - Python Versions

.. image:: https://img.shields.io/pypi/l/valu3s.framework.svg
    :target: https://pypi.python.org/pypi/valu3s.framework/
    :alt: License


================
valu3s.framework
================

Tell me what your product does

Features
--------

- Can be bullet points


Examples
--------

This add-on can be seen in action at the following sites:
- Is there a page on the internet where everybody can see the features?


Documentation
-------------

Full documentation for end users can be found in the "docs" folder, and is also available online at http://docs.plone.org/foo/bar


Translations
------------

This product has been translated into

- Klingon (thanks, K'Plai)


Installation
------------

Install valu3s.framework by adding it to your buildout::

    [buildout]

    ...

    eggs =
        valu3s.framework


and then running ``bin/buildout``


Contribute
----------

- Issue Tracker: https://github.com/collective/valu3s.framework/issues
- Source Code: https://github.com/collective/valu3s.framework
- Documentation: https://docs.plone.org/foo/bar


Support
-------

If you are having issues, please let us know.
We have a mailing list located at: project@example.com


License
-------

The project is licensed under the GPLv2.
